---
layout: inner
position: right
title: 'Tower of Hanoi'
date: 2020-01-24 23:11:11 +0300
categories: development
tags: Html5 Canvas Animation
featured_image: '/img/posts/02-tower-of-hanoi.png'
project_link: 'https://gitlab.com/boraa/tower-of-hanoi'
button_icon: 'gitlab'
button_text: 'Visit Project'
lead_text: 'Tower of Hanoi Html5 canvas animation that visualizes minimum disk movements that uses the recursive algorithm.'
description: 'Tower Of Hanoi Html5 canvas animation example shows the minimum disk moves to reach the goal using recursive algorithm and html5 canvas to visualize disk moves'
last_modified_at: 2020-01-30T08:11:12+03:00
---

# Tower of Hanoi

![alt text](/img/posts/02-tower-of-hanoi.png "Tower of Hanoi")

Tower Of Hanoi Html5 Canvas Animation.

The Tower of Hanoi is a mathematical game or puzzle in which there are three rods and n number of disks; all of them are of different sizes.

The goal of the game is to move all of the disks on the first rod to the third rod with minimum moves.

Larger disk cannot be placed on the top of a smaller disk. The disks should be in ascending order of size on one rod. So the smallest disk should be at the top and the largest should be at the bottom.

This Tower of Hanoi Html5 canvas animation example shows the minimum moves to reach the goal of the game using recursive algorithm to choose the disks to move and html5 canvas to visualize the moves.

<a href="https://gitlab.com/boraa/tower-of-hanoi" class="project-link"><button class="btn btn-default btn-lg"><i class="fa fa-gitlab fa-lg"></i>Visit Project</button></a>
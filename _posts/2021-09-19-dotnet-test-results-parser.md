---
layout: inner
position: right
title: 'DotNet Core Test Results Parser'
date: 2021-09-19 21:55:15 +0300
categories: development
tags: Development DotNet
featured_image: '/img/posts/04-dotnet-test-results-parser.png'
project_link: 'https://github.com/QuickOrBeDead/Labo.DotnetTestResultParser'
button_icon: 'github'
button_text: 'Visit Project'
lead_text: '.Net Core Test Result Parser Global Tool for NUnit and XUnit.'
description: '.Net Core Test Result Parser Global Tool for NUnit and XUnit.'
last_modified_at: 2021-09-19T21:55:15+03:00
---

# DotNet Core Test Results Parser

![alt text](/img/posts/04-dotnet-test-results-parser.png "DotNet Core Test Results Parser")

.Net Core Test Result Parser Global Tool for NUnit and XUnit

## Supported Formats

- NUnit
- XUnit
- MsTest

<a href="https://github.com/QuickOrBeDead/Labo.DotnetTestResultParser" class="project-link"><button class="btn btn-default btn-lg"><i class="fa fa-gitlab fa-lg"></i>Visit Project</button></a>
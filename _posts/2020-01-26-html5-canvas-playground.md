---
layout: inner
position: left
title: 'Html5 Canvas Playground'
date: 2020-01-26 22:14:15 +0300
categories: development
tags: Html5 Canvas Playground
featured_image: '/img/posts/03-html5-canvas-playground.png'
project_link: 'https://gitlab.com/boraa/html5-canvas-playground'
button_icon: 'gitlab'
button_text: 'Visit Project'
lead_text: 'Html5 Canvas Playground web application. A code editor for html5 canvas drawing experiments. Write html5 canvas javascript codes and see the results of your experiment instantly.'
description: 'Html5 Canvas Playground is a web app for html5 canvas drawing experiments. Write javascript code in code editor and see results in preview pane immediately.'
last_modified_at: 2020-02-01T17:48:12+03:00
---

# Html5 Canvas Playground

![alt text](/img/posts/03-html5-canvas-playground.png "Html5 Canvas Playground")

Html5 Canvas Playground is web application that you can use as a playground for your html5 canvas drawing experiments. 

It is a code editor that you can write your html5 canvas javascript code and see the drawing result on the preview area instantly. 

So you can play with your html5 canvas codes and see the results of your work immediately and easily without the effort of building a web application.

## Features

- Works offline
- Autosave feature
- Code syntax highlighting
- Preview canvas drawing result
- Save canvas as image

<a href="https://gitlab.com/boraa/html5-canvas-playground" class="project-link"><button class="btn btn-default btn-lg"><i class="fa fa-gitlab fa-lg"></i>Visit Project</button></a>